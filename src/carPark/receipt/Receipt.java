package carPark.receipt;

import carPark.vehicle.*;

import java.io.PrintWriter;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.Random;
import java.util.Scanner;

/**
 * A model for the class Receipt - The holder of important information
 *
 * @author mat80
 * @version 1.0 (09/05/2019)
 */
public class Receipt {
    private String receiptCode;
    private String zoneName;
    private int spotID;
    private String licencePlate;
    private Class vehicleClass;
    private LocalDateTime startDateTime;
    private DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

    /**
     * Basic constructor for Receipt
     */
    public Receipt() {

    }

    /**
     * Constructor for Receipt
     *
     * @param zoneName     is the zone where the vehicle is parked
     * @param spotID       is the spot in the vehicle is parked
     * @param licencePlate is the licence plate of the parked vehicle
     * @param vehicleClass is the type of vehicle that is parked
     */
    public Receipt(String zoneName, int spotID, String licencePlate, Class vehicleClass) {
        this.zoneName = zoneName;
        this.spotID = spotID;

        this.licencePlate = licencePlate;
        this.vehicleClass = vehicleClass;
        receiptCode = generateString();
        startDateTime = LocalDateTime.now();
    }

    /**
     * This method generates a random receiptCode
     *
     * @return a string of random numbers and characters
     */
    private static String generateString() {
        Random secureRandom = new Random();
        byte[] token = new byte[5];
        secureRandom.nextBytes(token);
        return new BigInteger(1, token).toString(16); //hex encoding
    }

    /**
     * This method returns the type of vehicle saved in Receipt
     *
     * @return type of vehicle
     */
    public Class getVehicleClass() {
        return vehicleClass;
    }

    /**
     * This method returns the receipt code
     *
     * @return receipt code
     */
    public String getReceiptCode() {
        return receiptCode;
    }

    /**
     * This method returns the zone name
     *
     * @return zone name
     */
    public String getZoneName() {
        return zoneName;
    }

    /**
     * This method returns the spot number
     *
     * @return spot number
     */
    public int getSpotID() {
        return spotID;
    }

    /**
     * This method returns the start time when the Customer first parked their vehicle
     *
     * @return the time when Customer parked their vehicle
     */
    public LocalDateTime getStartDateTime() {
        return startDateTime;
    }

    /**
     * This method returns the licence plate of Customer vehicle
     *
     * @return licence plate
     */
    public String getLicencePlate() {
        return licencePlate;
    }

    /**
     * This method saves information about Receipt to a file
     *
     * @param outfile is the file the information is saved to
     */
    public void save(PrintWriter outfile) {
        if (outfile == null) {
            throw new IllegalArgumentException("Output file must not be null");
        }
        outfile.println("-----Receipt-----");
        outfile.println(receiptCode);
        outfile.println(zoneName);
        outfile.println(spotID);
        writeStartDateTime(outfile);
        outfile.println(vehicleClass.getName());
    }

    /**
     * This method prints the start time to a file
     *
     * @param outfile is the file the information is printed
     */
    private void writeStartDateTime(PrintWriter outfile) {
        outfile.println(startDateTime.getYear());
        outfile.println(startDateTime.getMonthValue());
        outfile.println(startDateTime.getDayOfMonth());
        outfile.println(startDateTime.getHour());
        outfile.println(startDateTime.getMinute());
        outfile.println(startDateTime.getSecond());
    }


    /**
     * This method loads information about Receipt
     *
     * @param infile is the file the information is gathered from
     */
    public void load(Scanner infile) {
        if (infile == null) {
            throw new IllegalArgumentException("Infile must not be null");
        }
        receiptCode = infile.next();
        zoneName = infile.next();
        spotID = infile.nextInt();
        readStartDateTime(infile);


        String typeOfVehicle = infile.next();
        setTypeOfVehicle(typeOfVehicle);
    }

    /**
     * This method reads the start time
     *
     * @param infile is the file where the information about start time is loaded
     */
    private void readStartDateTime(Scanner infile) {
        int year = infile.nextInt();
        int month = infile.nextInt();
        int day = infile.nextInt();
        int hour = infile.nextInt();
        int minute = infile.nextInt();
        int second = infile.nextInt();

        startDateTime = LocalDateTime.of(year, month, day, hour, minute, second);

    }

    /**
     * This method sets the type of vehicle
     *
     * @param typeOfVehicle is the type of vehicle
     */
    private void setTypeOfVehicle(String typeOfVehicle) {
        if (typeOfVehicle.equals("carPark.vehicle.StandardVehicle")) {
            vehicleClass = StandardVehicle.class;
        }
        if (typeOfVehicle.equals("carPark.vehicle.HigherVehicle")) {
            vehicleClass = HigherVehicle.class;
        }
        if (typeOfVehicle.equals("carPark.vehicle.LongerVehicle")) {
            vehicleClass = LongerVehicle.class;
        }
        if (typeOfVehicle.equals("carPark.vehicle.Coache")) {
            vehicleClass = Coache.class;
        }
        if (typeOfVehicle.equals("carPark.vehicle.Motorbike")) {
            vehicleClass = Motorbike.class;
        }
    }

    /**
     * This method returns information about Receipt
     *
     * @return a string of information
     */
    @Override
    public String toString() {

        StringBuilder receiptString = new StringBuilder();
        receiptString.append(" \nReceipt{ \n");
        receiptString.append("Receipt code: " + receiptCode + " \n");
        receiptString.append("Location: " + zoneName + ", Spot number: " + spotID + "\n");
        receiptString.append("Start time: " + timeFormatter.format(startDateTime) + "\n");
        receiptString.append("}");

        return receiptString.toString();
    }


    /**
     * This method compares two object two one another
     *
     * @param o is the object being compared
     * @return true/false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Receipt receipt = (Receipt) o;
        return Objects.equals(receiptCode, receipt.receiptCode);
    }


    @Override
    public int hashCode() {

        return Objects.hash(receiptCode);
    }
}