package carPark;

import carPark.receipt.Receipt;
import carPark.vehicle.*;
import carPark.zone.Zone;

import java.io.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * A model for the class CarPark - Everything needed to run the application
 *
 * @author mat80
 * @version 1.0 (09/05/2019)
 */
public class CarPark {

    //---------CarPark Objects---------
    private ArrayList<Zone> zones = new ArrayList<>();
    private ArrayList<Receipt> activeReceipts = new ArrayList<>();


    /**
     * Constructor for CarPark
     */
    public CarPark(){

    }
    //---------------------------------------------INITIALIZE LOGIC-----------------------------------------------------

    /**
     * This method sets up everything needed to run the car park
     */
    public void initialize() {

        tryLoadZones();

        if (zones.isEmpty()) {
            defaultZoneSetup();
            trySaveZones();
        }
    }

    /**
     * This method sets default zones to the car park and adds them to a list of zones
     */
    private void defaultZoneSetup() {
        ArrayList<Vehicle> typeOfVehiclesSupport = new ArrayList<>();


        typeOfVehiclesSupport.add(new StandardVehicle());
        typeOfVehiclesSupport.add(new HigherVehicle());
        Zone zone1 = new Zone("Zone1", 5, typeOfVehiclesSupport, 1);

        typeOfVehiclesSupport.clear();
        typeOfVehiclesSupport.add(new LongerVehicle());
        Zone zone2 = new Zone("Zone2", 5, typeOfVehiclesSupport, 1.5);

        typeOfVehiclesSupport.clear();
        typeOfVehiclesSupport.add(new Coache());
        Zone zone3 = new Zone("Zone3", 5, typeOfVehiclesSupport, 2);

        typeOfVehiclesSupport.clear();
        typeOfVehiclesSupport.add(new StandardVehicle());
        Zone zone4 = new Zone("Zone4", 1, typeOfVehiclesSupport, 1);

        typeOfVehiclesSupport.clear();
        typeOfVehiclesSupport.add(new Motorbike());
        Zone zone5 = new Zone("Zone5", 5, typeOfVehiclesSupport, 0.5);


        zones.add(zone4); //Want zone 4 to be the priority zone
        zones.add(zone1);
        zones.add(zone2);
        zones.add(zone3);
        zones.add(zone5);
    }




    //---------------------------------------------CAR PARK LOGIC-------------------------------------------------------
    /**
     * This method gets all the zones in the car park
     */
    public ArrayList<Zone> getZones() {
        return zones;
    }

    /**
     * This method displays all the zones
     */
    public void zoneDisplay() {
        for (Zone zone : zones) {
            System.out.println(zone);
        }
    }

    /**
     * This method finds the first available spot and parks the vehicle
     *
     * @param vehicle is the vehicle being parked
     * @return receipt
     */
    public Receipt parkVehicleAsCustomer(Vehicle vehicle) {
        System.out.println("Searching for zone...");
        for (Zone zone : zones) { //Looks trough zones

            for (Vehicle vehicleType : zone.getTypeOfAllowedVehicles()) { //Looks trough type of vehicles allowed in the zone

                if (vehicle.getClass().equals(vehicleType.getClass())) { //Checks if the vehicle is of the correct type
                    System.out.println("Found " + zone.getName());

                    int spotID = zone.getFreeParkingSpot();

                    if (spotID != -1) {
                        Receipt receipt = parkVehicle(vehicle, zone, spotID);
                        return receipt;
                    } else{
                        System.out.println("No parking spots available");
                    }
                }
            }
        }
        return null;
    }

    /**
     * This method parks the vehicle and returns a receipt
     *
     * @param vehicle is the vehicle being parked
     * @param zone is the zone where the vehicle is being parked
     * @param spotID is the spot in the zone where the vehicle is being parked
     * @return receipt
     */
    private Receipt parkVehicle(Vehicle vehicle, Zone zone, int spotID) {
        zone.getParkingSpots().get(spotID).setVehicle(vehicle);
        System.out.println("Parked in spot: " + (spotID+1));

        Receipt receipt = new Receipt(zone.getName(), spotID+1, vehicle.getLicencePlate(), vehicle.getClass());

        trySaveZones();


        return receipt;
    }

    /**
     * This method parks the vehicle and returns a receipt
     *
     * @param vehicle is the vehicle being parked
     * @param zoneName is the zone where the vehicle is being parked
     * @param spotID is the spot in the zone where the vehicle is being parked
     * @return receipt
     */
    public Receipt parkVehicleAsAttendant(Vehicle vehicle, String zoneName, int spotID) {
        for (Zone zone : zones) {                                                          //Looks trough zones
            if (zone.getName().equals(zoneName)) {                                         //Check if name matches with any zone names
                for (Vehicle vehicleType : zone.getTypeOfAllowedVehicles()) {                     //Looks trough type of vehicles allowed in the zone
                    if (vehicle.getClass().equals(vehicleType.getClass())) {               //Checks if the vehicle is of the correct type

                        System.out.println("Found " + zone.getName());
                        spotID -=1;                                                        //We assume we start from spot number 1, but in reality we start at 0
                        if (zone.getParkingSpots().get(spotID).getVehicle() == null) {
                            Receipt receipt = parkVehicle(vehicle, zone, spotID);
                            return  receipt;
                        } else{
                            System.out.println("Parking spot occupied");
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * This method looks for the type of zone your vehicle is allowed to park in
     *
     * @param vehicle is the vehicle used for reference
     */
    public void searchForAllowedZonesToParkIn(Vehicle vehicle) {
        for (Zone zone : zones) {
            for (Vehicle vehicleType : zone.getTypeOfAllowedVehicles()) {
                if (vehicleType.getClass().equals(vehicle.getClass())) {
                    System.out.println("You can park in: ");
                    System.out.println(zone);
                }
            }
        }
    }

    /**
     * This method removes the vehicle from the parking spot and the receipt
     *
     * @param receipt is the receipt being removed
     */
    public void removeVehicleAndReceipt(Receipt receipt) {
        String zoneName = receipt.getZoneName();
        int spotID = receipt.getSpotID();

        for (Zone zone : zones) {
            if (zone.getName().equals(zoneName)) {
                zone.removeVehicle(spotID);
                removeReceipt(receipt);
            }
        }
        trySaveZones();
    }

    /**
     * This method removes the receipt from active receipts
     *
     * @param receipt is the receipt being removed
     */
    private void removeReceipt(Receipt receipt) {
        Receipt receiptToRemove = null;
        for (Receipt receiptInArray : activeReceipts) {
            if (receipt.getReceiptCode().equals(receiptInArray.getReceiptCode())) {
                receiptToRemove = receiptInArray;
            }
        }
        activeReceipts.remove(receiptToRemove);

    }

    /**
     * This method adds a receipt to a list of active receipts
     *
     * @param receipt is the receipt being added
     */
    public void addActiveReceipt(Receipt receipt) {
        activeReceipts.add(receipt);
    }


    /**
     * This method returns a list of active receipts
     *
     * @return a list of active receipts
     */
    public ArrayList<Receipt> getActiveReceipts() {
        return activeReceipts;
    }

    /**
     * This method searches for a receipt in the list of active receipts
     *
     * @param receiptCode is the receipt code used to look for a match
     * @return receipt if found
     */
    public Receipt searchForReceipt(String receiptCode) {
        Receipt receiptTemp = null;
        for (Receipt receipt : getActiveReceipts()) {
            if (receipt.getReceiptCode().equals(receiptCode)) {
                receiptTemp = receipt;
            }
        }
        return receiptTemp;
    }

    /**
     * This method calculates the total amount of units a Customer has to pay
     *
     * @param receipt is used to get information
     * @return total units to pay
     */
    public double getTotalUnits(Receipt receipt) {
        double totalTimeParked = getTotalTimeParked(receipt);
        double zonePrice = 0;
        String zoneName = receipt.getZoneName();

        for (Zone zone : zones) {
            if (zoneName.equals(zone.getName())) {
                zonePrice = zone.getPriceHour();
            }
        }

        double totalUnits = totalTimeParked * zonePrice;
        return totalUnits;

    }

    /**
     * This method calculates the total time a Customer has stayed in the car park
     *
     * @param receipt is used for information
     * @return the total time parked in hours
     */
    private double getTotalTimeParked(Receipt receipt) {
        LocalDateTime startTime = receipt.getStartDateTime();
        LocalDateTime endTime = LocalDateTime.now();

        Duration period = Duration.between(startTime, endTime);

        return Math.abs(period.toHours() + 1); //if you have stayed for 1 hour 1 sec, you got to pay for 2 hours
    }





    //---------------------------------------------LOADING & SAVING-----------------------------------------------------
    /**
     * This method tries to save Zones
     */
    public void trySaveZones() {
        try {
            saveZones();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * This method saves zones to a txt file
     * @throws IOException
     */
    private void saveZones() throws IOException {
        String outfileName = "zoneData.txt";

        try (FileWriter fw = new FileWriter(outfileName);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter outfile = new PrintWriter(bw)) {

            for (Zone zone : zones) {
                zone.save(outfile);
            }
        }
    }

    /**
     * This method tries to load Zones
     */
    private void tryLoadZones() {
        try {
            loadZones();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * This method loads zones from a txt file
     * @throws IOException
     */
    public void loadZones() throws IOException {
        String filename = "zoneData.txt";
        try (FileReader fr = new FileReader(filename);
             BufferedReader br = new BufferedReader(fr);
             Scanner infile = new Scanner(br)) {

            while (infile.hasNext()) {
                Zone zone = new Zone();
                zone.load(infile);
                zones.add(zone);
            }
        }
    }
}
