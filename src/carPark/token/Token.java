package carPark.token;

import java.io.PrintWriter;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

/**
 * A model for the class Token - An important item used to exit the car park
 *
 * @author mat80
 * @version 1.0 (09/05/2019)
 */
public class Token {
    private int maxExitTimeInMinutes = 15;
    private DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
    private LocalDateTime startTime = null;

    /**
     * Constructor for class Token
     * Taking a sample from current time
     */
    public Token() {
    }

    /**
     * This method resets the timer and prints the exit time
     */
    public void resetTimer(){
        startTime = LocalDateTime.now();

        //Prints time used to exit
        LocalDateTime exitTime = startTime.plusMinutes(maxExitTimeInMinutes);
        String exitTimeString = timeFormatter.format(exitTime);
        System.out.println("Exit before: " + exitTimeString);
    }


    /**
     * This method is checking whether or not the user have used more time than the Token have allowed
     * @return true/false
     */
    public boolean hasExitGarageInTime() {
        long exitTime = exitTimer();
        boolean hasExitGarage;
        hasExitGarage = exitTime <= maxExitTimeInMinutes;
        return hasExitGarage;
    }

    /**
     * This method calculates the time between startTime to endTime
     *
     * @return total time in minutes
     */
    private long exitTimer() {
        LocalDateTime endTime = LocalDateTime.now();
        Duration exit = Duration.between(startTime, endTime);

        long exitTime = Math.abs(exit.toMinutes());
        return exitTime;
    }

    /**
     * This method saves information about Receipt to a file
     *
     * @param outfile is the file the information is saved to
     */
    public void save(PrintWriter outfile) {
        if (outfile == null) {
            throw new IllegalArgumentException("Output file must not be null");
        }
        outfile.println("-----Token-----");
        writeStartTime(outfile);
    }

    /**
     * This method prints the start time to a file
     *
     * @param outfile is the file the information is printed
     */
    private void writeStartTime(PrintWriter outfile) {
        outfile.println(startTime.getYear());
        outfile.println(startTime.getMonthValue());
        outfile.println(startTime.getDayOfMonth());
        outfile.println(startTime.getHour());
        outfile.println(startTime.getMinute());
        outfile.println(startTime.getSecond());
    }


    /**
     * This method loads information about Receipt
     *
     * @param infile is the file the information is gathered from
     */
    public void load(Scanner infile) {
        if (infile == null) {
            throw new IllegalArgumentException("Infile must not be null");
        }
        readStartTime(infile);
    }


    /**
     * This method reads the start time
     *
     * @param infile is the file where the information about start time is loaded
     */
    private void readStartTime(Scanner infile) {
        int year = infile.nextInt();
        int month = infile.nextInt();
        int day = infile.nextInt();
        int hour = infile.nextInt();
        int minute = infile.nextInt();
        int second = infile.nextInt();

        startTime = LocalDateTime.of(year, month, day, hour, minute, second);

    }

}
