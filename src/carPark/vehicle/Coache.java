package carPark.vehicle;

/**
 * A model for the class Coache - A big vehicle
 *
 * @author mat80
 * @version 1.0 (09/05/2019)
 */
public class Coache extends Vehicle {

    /**
     * Basic constructor for Coache
     */
    public Coache() {
        super();
    }

    /**
     * Constructor for Coache
     *
     * @param licencePlate is the licence plate of the Vehicle
     */
    public Coache(String licencePlate) {
        super(licencePlate);
    }

    /**
     * This method returns information about Coache
     *
     * @return a string of information
     */
    @Override
    public String toString() {
        StringBuilder coacheInformation = new StringBuilder();
        coacheInformation.append("Coache{");
        coacheInformation.append("Licence plate: " + licencePlate);
        coacheInformation.append("}");
        return coacheInformation.toString();
    }
}
