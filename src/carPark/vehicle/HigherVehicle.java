package carPark.vehicle;

/**
 * A model for the class HigherVehicle - A high vehicle
 *
 * @author mat80
 * @version 1.0 (09/05/2019)
 */
public class HigherVehicle extends Vehicle {

    /**
     * Basic constructor for HigherVehicle
     */
    public HigherVehicle() {

    }

    /**
     * Constructor for HigherVehicle
     *
     * @param licencePlate is the licence plate of the Vehicle
     */
    public HigherVehicle(String licencePlate) {
        super(licencePlate);
    }

    /**
     * This method returns information about HigherVehicle
     *
     * @return a string of information
     */
    @Override
    public String toString() {
        StringBuilder higherVehicleInformation = new StringBuilder();
        higherVehicleInformation.append("Higher vehicle{ ");
        higherVehicleInformation.append("Licence plate: " + licencePlate);
        higherVehicleInformation.append(" }");
        return higherVehicleInformation.toString();
    }
}
