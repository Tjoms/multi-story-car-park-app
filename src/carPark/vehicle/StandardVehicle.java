package carPark.vehicle;

/**
 * A model for the class StandardVehicle - A normal vehicle
 *
 * @author mat80
 * @version 1.0 (09/05/2019)
 */
public class StandardVehicle extends Vehicle {


    /**
     * Basic constructor for StandardVehicle
     */
    public StandardVehicle() {
        super();
    }

    /**
     * Constructor for StandardVehicle
     *
     * @param licencePlate is the licence plate of the Vehicle
     */
    public StandardVehicle(String licencePlate) {
        super(licencePlate);
    }

    /**
     * This method returns information about StandardVehicle
     *
     * @return a string of information
     */
    @Override
    public String toString() {
        StringBuilder standardVehicleInformation = new StringBuilder();
        standardVehicleInformation.append("Standard vehicle{ ");
        standardVehicleInformation.append("Licence plate: " + licencePlate);
        standardVehicleInformation.append(" }");
        return standardVehicleInformation.toString();
    }
}
