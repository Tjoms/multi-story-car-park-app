package carPark.vehicle;

/**
 * A model for the class LongerVehicle - A long vehicle
 *
 * @author mat80
 * @version 1.0 (09/05/2019)
 */
public class LongerVehicle extends Vehicle {

    /**
     * Basic constructor for LongerVehicle
     */
    public LongerVehicle() {

    }

    /**
     * Constructor for LongerVehicle
     *
     * @param licencePlate is the licence plate of the Vehicle
     */
    public LongerVehicle(String licencePlate) {
        super(licencePlate);
    }

    /**
     * This method returns information about LongerVehicle
     *
     * @return a string of information
     */
    @Override
    public String toString() {
        StringBuilder longerVehicleInformation = new StringBuilder();
        longerVehicleInformation.append("Longer vehicle{ ");
        longerVehicleInformation.append("Licence plate: " + licencePlate);
        longerVehicleInformation.append(" }");
        return longerVehicleInformation.toString();
    }
}
