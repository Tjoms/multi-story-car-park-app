package carPark.vehicle;

/**
 * A model for the class Motorbike - A motorbike
 *
 * @author mat80
 * @version 1.0 (09/05/2019)
 */
public class Motorbike extends Vehicle{

    /**
     * Basic constructor for Motorbike
     */
    public Motorbike(){

    }

    /**
     * Constructor for Motorbike
     *
     * @param licencePlate is the licence plate of the Vehicle
     */
    public Motorbike(String licencePlate){
        super(licencePlate);
    }

    /**
     * This method returns information about Motorbike
     *
     * @return a string of information
     */
    @Override
    public String toString() {
        StringBuilder motorbikeInformation = new StringBuilder();
        motorbikeInformation.append("Motorbike{ ");
        motorbikeInformation.append("Licence plate: " + licencePlate);
        motorbikeInformation.append(" }");
        return motorbikeInformation.toString();
    }
}
