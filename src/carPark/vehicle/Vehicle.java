package carPark.vehicle;

import java.io.PrintWriter;
import java.util.Scanner;

/**
 * A model for the class Vehicle - The body of every type of vehicle
 *
 * @author mat80
 * @version 1.0 (09/05/2019)
 */
public class Vehicle{
    String licencePlate;
    private double height;
    private double length;

    /**
     * Basic constructor for Vehicle
     */
    public Vehicle(){

    }

    /**
     * Constructor for Vehicle
     *
     * @param licencePlate is the licence plate of the Vehicle
     */
    public Vehicle(String licencePlate){
        this.licencePlate = licencePlate;
    }

    /**
     * Advanced constructor for Vehicle
     *
     * @param licencePlate is the licence plate of the Vehicle
     * @param height is the height of the Vehicle
     * @param length is the length of the Vehicle
     */
    public Vehicle(String licencePlate, double height, double length){
        this.licencePlate = licencePlate;
        this.height = height;
        this.length = length;

    }

    /**
     * This method returns a type of vehicle
     *
     * @param vehicle is the reference vehicle
     * @return the type of vehicle depending on the reference vehicle
     */
    public Vehicle getTypeOfVehicle(Vehicle vehicle) {
        Vehicle typeOfVehicle = null;

        double height = vehicle.getHeight();
        double length = vehicle.getLength();
        String licencePlate = vehicle.getLicencePlate();

        //Set type of vehicle
        if (height == 0 && length == 0) {
            typeOfVehicle = new Motorbike(licencePlate);
        } else if (height <= 2 && length < 5) {
            typeOfVehicle = new StandardVehicle(licencePlate);
        } else if (height > 2 && height < 3 && length < 5) {
            typeOfVehicle = new HigherVehicle(licencePlate);
        } else if (length >= 5.1 && height < 3 && length <= 6) {
            typeOfVehicle = new LongerVehicle(licencePlate);
        } else if (length <= 15) {
            typeOfVehicle = new Coache(licencePlate);
        } else{
            System.out.println("This vehicle does not exist");
        }

        return typeOfVehicle;
    }
    /**
     * This method sets a licence plate to the Vehicle
     *
     * @param licencePlate is the new licence plate
     */
    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }


    /**
     * This method returns the licence plate of the Vehicle
     *
     * @return licence plate
     */
    public String getLicencePlate() {
        return licencePlate ;
    }

    /**
     * This method returns the height of the Vehicle
     *
     * @return height of Vehicle
     */
    public double getHeight() {
        return height;
    }

    /**
     * This method returns the length of the Vehicle
     *
     * @return length of Vehicle
     */
    public double getLength() {
        return length;
    }

    /**
     * This method saves information about Vehicle to a file
     *
     * @param infile is the file that the information is being saved to
     */
    public void save(PrintWriter infile){
        if(infile == null){
            throw new IllegalArgumentException("Output infile must not be null");
        }
        infile.println(licencePlate);

    }

    /**
     * This method loads information about Vehicle from a file
     *
     * @param file is the file the information is being loaded from
     */
    public void load(Scanner file){
        if (file == null) {
            throw new IllegalArgumentException("Input file must not be null");
        }
        licencePlate = file.next();
    }

    /**
     * This method returns information about Vehicle
     *
     * @return a string of information
     */
    @Override
    public String toString() {
        return "Vehicle{" +
                "licencePlate='" + licencePlate + '\'' +
                '}';
    }
}
