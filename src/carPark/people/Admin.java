package carPark.people;

import java.io.PrintWriter;
import java.util.Scanner;

/**
 * A model for the class Admin - The boss
 *
 * @author mat80
 * @version 1.0 (09/05/2019)
 */
public class Admin extends Person {

    /**
     *Basic constructor for Admin
     */
    public Admin(){
        super();
    }

    /**
     * Constructor for Admin
     *
     * @param name is the username of Admin
     * @param password is the password connected to the username
     */
    public Admin(String name, String password){
        super(name, password);
    }

    /**
     * This method saves information about Admin
     *
     * @param outfile is the file the information is saved to
     */
    @Override
    public void save(PrintWriter outfile){
        if(outfile == null){
            throw new IllegalArgumentException("Output file must not be null");
        }
        outfile.println("-----Admin-----");
        super.save(outfile);
    }

    /**
     * This method loads information about Admin
     *
     * @param infile is the file the information is loaded from
     */
    @Override
    public void load(Scanner infile){
        super.load(infile);
    }
}
