package carPark.people;

import carPark.vehicle.*;

import java.io.PrintWriter;
import java.util.Objects;
import java.util.Scanner;

/**
 * A model for the class Person - The solid body for every type of people
 *
 * @author mat80
 * @version 1.0 (09/05/2019)
 */
public class Person {
    private String name;
    private String password;
    private Vehicle vehicle;

    /**
     * Basic constructor for Person
     */
    Person(){}

    /**
     * Constructor for Person
     *
     * @param name is the username of the Person
     * @param password is the password connected to the username
     */
    Person(String name, String password){
        this.name = name;
        this.password = password;
    }

    /**
     * This method returns the Persons vehicle
     *
     * @return vehicle
     */
    public Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * This method gives Person a vehicle
     *
     * @param vehicle is the new vehicle
     */
    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    /**
     * This method returns the Persons username
     *
     * @return username
     */
    public String getName() {
        return name;
    }

    /**
     * This method returns the Persons password
     *
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * This method sets the type of vehicle
     *
     * @param infile is the file used to load information from
     */
    private void setTypeOfVehicle(Scanner infile) {
        String typeOfVehicle = infile.next();
        switch (typeOfVehicle){
            case "carPark.vehicle.StandardVehicle":
                vehicle = new StandardVehicle();
                break;
            case "carPark.vehicle.LongerVehicle":
                vehicle = new LongerVehicle();
                break;
            case "carPark.vehicle.HigherVehicle":
                vehicle = new HigherVehicle();
                break;
            case "carPark.vehicle.Coache":
                vehicle = new Coache();
                break;
            case"carPark.vehicle.Motorbike":
                vehicle = new Motorbike();
                break;
        }
    }

    /**
     * This method saves information about Person to a file
     *
     * @param outfile is the file the information is saved to
     */
    public void save(PrintWriter outfile){
        if(outfile == null){
            throw new IllegalArgumentException("Output file must not be null");
        }

        outfile.println(name);
        outfile.println(password);
        if(vehicle != null){
            outfile.println("-----Vehicle-----");
            outfile.println(vehicle.getClass().getName());
            vehicle.save(outfile);
        }

    }

    /**
     * This method loads information about Person
     *
     * @param infile is the file the information is loaded from
     */
    public void load(Scanner infile){
        name = infile.next();
        password = infile.next();
        if(infile.hasNext("-----Vehicle-----")){
            infile.next();

            setTypeOfVehicle(infile);

            vehicle.load(infile);
        }
    }

    /**
     * This method compares two Persons
     *
     * @param o is the people being compared
     * @return true/false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(name, person.name) &&
                Objects.equals(password, person.password);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, password);
    }
}
