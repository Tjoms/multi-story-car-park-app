package carPark.people;

import carPark.token.Token;
import carPark.vehicle.HigherVehicle;
import carPark.vehicle.LongerVehicle;
import carPark.vehicle.StandardVehicle;

import java.io.PrintWriter;
import java.util.Scanner;

/**
 * A model for the class Attendant - Workers in the car park
 *
 * @author mat80
 * @version 1.0 (09/05/2019)
 */
public class Attendant extends Person {

    private Token token = null;
    private String customerName;

    /**
     * Basic constructor for Attendant
     */
    public Attendant() {
        super();
    }

    /**
     * Constructor for Attendant
     *
     * @param name     is the name of the Attendant
     * @param password is the password of the Attendant
     */
    public Attendant(String name, String password) {
        super(name, password);
    }


    /**
     * This method checks whether or not the Attendant can park the vehicle
     *
     * @param vehicle is the vehicle used to check whether or not the Attendant can park it
     * @return true/false
     */
    public static boolean canParkThisVehicle(Class vehicle) {
        boolean canPark = false;

        if (vehicle == StandardVehicle.class) {
            canPark = true;
        }
        if (vehicle == HigherVehicle.class) {
            canPark = true;
        }
        if (vehicle == LongerVehicle.class) {
            canPark = true;
        }

        return canPark;
    }

    /**
     * This method returns the name of the Attendants customer
     *
     * @return customer name
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * This method gives the Attendant a customer
     *
     * @param customerName
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * This method gives a token to the Attendant
     *
     * @param token is the token given to the Attendant
     */
    public void setToken(Token token) {
        this.token = token;
    }

    /**
     * This method returns the Attendants token
     *
     * @return token that belongs to Attendant
     */
    public Token getToken() {
        return token;
    }

    /**
     * This method saves information about Attendant
     *
     * @param outfile is the file which the information is saved
     */
    @Override
    public void save(PrintWriter outfile) {
        if (outfile == null) {
            throw new IllegalArgumentException("Output file must not be null");
        }
        outfile.println("-----Attendant-----");
        super.save(outfile);
        outfile.println(customerName);

        if (token != null) {
            token.save(outfile);
        }

    }

    /**
     * This method loads information about Attendant
     *
     * @param infile is the file used to get information
     */
    @Override
    public void load(Scanner infile) {
        super.load(infile);
        customerName = infile.next();

        if (infile.hasNext("-----Token-----")) {
            infile.next();
            token = new Token();
            token.load(infile);
        }
    }
}
