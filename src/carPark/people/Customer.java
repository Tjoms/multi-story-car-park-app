package carPark.people;

import carPark.receipt.Receipt;
import carPark.token.Token;

import java.io.PrintWriter;
import java.util.Scanner;

/**
 * A model for the class Customer - A people using the car park and its features
 *
 * @author mat80
 * @version 1.0 (09/05/2019)
 */
public class Customer extends Person {

    private Receipt receipt;
    private boolean isDisabled;
    private Token token = null;

    /**
     * Basic constructor for Customer
     */
    public Customer() {
        super();
    }

    /**
     * Constructor for Customer
     *
     * @param name is the name of the Customer
     * @param password is the password of the Customer
     * @param isDisabled is a boolean saying whether or not the Customer is disabled
     */
    public Customer(String name, String password, boolean isDisabled) {
        super(name, password);
        this.isDisabled = isDisabled;
    }

    /**
     * This method returns the Customer receipt
     *
     * @return receipt
     */
    public Receipt getReceipt() {
        return receipt;
    }

    /**
     * This method gives a receipt to the Customer
     *
     * @param receipt is the receipt given to the Customer
     */
    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }

    /**
     * This method returns a boolean depending on whether or not the Customer is disabled
     *
     * @return true/false
     */
    public boolean isDisabled() {
        return isDisabled;
    }

    /**
     * This method gives a token to the Customer
     *
     * @param token is the token given to the Customer
     */
    public void setToken(Token token) {
        this.token = token;
    }

    /**
     * This method saves information about Customer
     *
     * @param outfile is the file the information is being saved to
     */
    @Override
    public void save(PrintWriter outfile) {
        if (outfile == null) {
            throw new IllegalArgumentException("Output file must not be null");
        }
        outfile.println("-----Customer-----");
        outfile.println(isDisabled);
        super.save(outfile);
        if (receipt != null) {
            receipt.save(outfile);
        }
        if(token != null){
            token.save(outfile);
        }
    }

    /**
     * This method loads information about Customer
     *
     * @param infile is the file the information is being loaded from
     */
    @Override
    public void load(Scanner infile) {
        isDisabled = infile.nextBoolean();
        super.load(infile);
        if (infile.hasNext("-----Receipt-----")) {
            infile.next(); //clear "-----Receipt-----"
            receipt = new Receipt();
            receipt.load(infile);
        }
        if(infile.hasNext("-----Token-----")){
            infile.next();
            token = new Token();
            token.load(infile);
        }
    }

}
