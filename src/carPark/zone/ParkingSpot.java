package carPark.zone;

import carPark.vehicle.*;

import java.io.PrintWriter;
import java.util.Scanner;

public class ParkingSpot {

    private int spotID;
    private Vehicle vehicle;

    /**
     * Basic constructor for ParkingSpot
     */
    ParkingSpot() {
    }

    /**
     * Constructor for ParkingSpot
     *
     * @param spotID is the spot number
     * @param vehicle is the vehicle placed in the spot
     */
    ParkingSpot(int spotID, Vehicle vehicle) {
        this.spotID = spotID;
        this.vehicle = vehicle;
    }

    /**
     * This method returns spot number
     *
     * @return spot number
     */
    public int getSpotID() {
        return spotID;
    }

    /**
     * This method sets the spot number
     *
     * @param spotID is the new spot number
     */
    public void setSpotID(int spotID) {
        this.spotID = spotID;
    }

    /**
     * This method returns a vehicle placed in the ParkingSpot
     *
     * @return vehicle if found
     */
    public Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * This mehtod sets a vehicle to ParkingSpot
     *
     * @param vehicle is the vehicle being parked in the spot
     */
    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }


    /**
     * This method saves information about ParkingSpot
     *
     * @param outfile is the file information is saved to
     */
    public void save(PrintWriter outfile) {
        if (outfile == null) {
            throw new IllegalArgumentException("Outfile must not be null");
        }
        outfile.println(spotID);
        if (vehicle != null) {
            vehicle.save(outfile);
            outfile.println(vehicle.getClass().getName());
        } else {
            outfile.println("Vehicle_Null");
        }

    }

    /**
     * This method loads information about ParkingSpot
     *
     * @param infile is where the information is gathered
     */
    public void load(Scanner infile) {
        if (infile == null) {
            throw new IllegalArgumentException("Infile must not be null");
        }
        spotID = infile.nextInt();
        String licencePlate = infile.next();

        if (!licencePlate.equals("Vehicle_Null")) {
            String typeOfVehicle =  infile.next();
            setTypeOfVehicle(typeOfVehicle, licencePlate);

            System.out.println(vehicle);
        }
    }

    /**
     * This method sets the type of vehicle
     *
     * @param typeOfVehicle is the type of vehicle
     */
    private void setTypeOfVehicle(String typeOfVehicle, String licencePlate) {
        if(typeOfVehicle.equals("carPark.vehicle.StandardVehicle")){
            vehicle = new StandardVehicle(licencePlate);
        }
        if(typeOfVehicle.equals("carPark.vehicle.HigherVehicle")){
            vehicle =new HigherVehicle(licencePlate);
        }
        if(typeOfVehicle.equals("carPark.vehicle.LongerVehicle")){
            vehicle =new LongerVehicle(licencePlate);
        }
        if(typeOfVehicle.equals("carPark.vehicle.Coache")){
            vehicle =new Coache(licencePlate);
        }
        if(typeOfVehicle.equals("carPark.vehicle.Motorbike")){
            vehicle =new Motorbike(licencePlate);
        }
    }

    /**
     * This method returns information about ParkingSpot
     *
     * @return a string of information
     */
    @Override
    public String toString() {
        StringBuilder parkingSpotString = new StringBuilder();
        parkingSpotString.append("\n" + "Parking spot: " + spotID);
        if(vehicle != null) {
            parkingSpotString.append(", " + vehicle);
        } else{
            parkingSpotString.append(", No Vehicle Parked");
        }
        return parkingSpotString.toString();
    }
}
