package carPark.zone;

import carPark.vehicle.*;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Zone {

    private String name;
    private double priceHour = 0.0;
    private int totalParkingSpots;
    private ArrayList<ParkingSpot> parkingSpots = new ArrayList<>();
    private ArrayList<Vehicle> typeOfAllowedVehicles = new ArrayList<>();

    /**
     * Basic constructor for Zone
     * */
    public Zone(){

    }

    /**
     * Constructor for Zone
     *
     * @param name is the name of the Zone
     * @param totalParkingSpots is the total parking spots in the Zone
     * @param typeOfAllowedVehicles is the allowed type of vehicles in the Zone
     * @param priceHour is the price/hour in the Zone
     */
    public Zone(String name, int totalParkingSpots, ArrayList<Vehicle> typeOfAllowedVehicles, double priceHour) {
        this.name = name;
        this.priceHour = priceHour;
        this.typeOfAllowedVehicles.addAll(typeOfAllowedVehicles);
        this.totalParkingSpots = totalParkingSpots;

        setTotalParkingSpots(totalParkingSpots);
    }

    /**
     * This method creates all the parking spots for Zone
     *
     * @param totalParkingSpots is the total amount of parking spots being created
     */
    private void setTotalParkingSpots(int totalParkingSpots) {

        for (int currentSpots = 0; currentSpots < totalParkingSpots; currentSpots++) {
            ParkingSpot parkingSpot = new ParkingSpot(currentSpots+1, null);
            parkingSpots.add(parkingSpot);
        }
    }

    /**
     * This method returns Zone name
     *
     * @return name of Zone
     */
    public String getName() {
        return name;
    }

    /**
     * This method returns Zone price/hour
     *
     * @return price/hour of Zone
     */
    public double getPriceHour() {
        return priceHour;
    }


    /**
     *This method sets Zone price/hour
     *
     * @param priceHour is the new price
     */
    public void setPriceHour(double priceHour) {
        this.priceHour = priceHour;
    }

    /**
     * This method sets Zone name
     *
     * @param name is the new Zone name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * This method returns all the parking spots in the Zone
     *
     * @return a list of parking spots
     */
    public ArrayList<ParkingSpot> getParkingSpots() {
        return parkingSpots;
    }

    /**
     * This method looks for the first available parking spot in the zone
     *
     * @return the first available parking spot
     */
    public int getFreeParkingSpot(){
        int parkingSpotNumber = -1; //There is no spot that has an spotID as -1
        for (ParkingSpot parkingSpot :  parkingSpots) {
            if (parkingSpot.getVehicle() == null) {
               parkingSpotNumber = parkingSpot.getSpotID()-1;
               System.out.println("Parking spot found");
                break;
            }
        }
        return parkingSpotNumber;
    }

    /**
     * This method removes a vehicle from a spot
     *
     * @param spotID is the spot the vehicle is being removed from
     */
    public void removeVehicle(int spotID) {
        parkingSpots.get(spotID-1).setVehicle(null);
    }

    /**
     * This method returns a list of the allowed type of vehicles in the Zone
     *
     * @return a list of vehicles allowed to park in the Zone
     */
    public ArrayList<Vehicle> getTypeOfAllowedVehicles() {
        return typeOfAllowedVehicles;
    }

    /**
     * This method saves information about the Zone
     *
     * @param outfile is the file the information is saved to
     */
    public void save(PrintWriter outfile){
        if(outfile == null){
            throw new IllegalArgumentException("Outfile must not be null");
        }
        outfile.println(name);
        outfile.println(priceHour);
        outfile.println(totalParkingSpots);

        for(Vehicle vehicle: typeOfAllowedVehicles){
            outfile.println(vehicle.getClass().getName());
        }
        for (ParkingSpot parkingSpot: parkingSpots){
            outfile.println("---ParkingSpot---");
            parkingSpot.save(outfile);

        }

        outfile.println("----------END OF ZONE----------");


    }

    /**
     * This method loads information about the Zone from a file
     *
     * @param infile is the file information is gathered from
     */
    public void load(Scanner infile){
        if (infile == null) {
            throw new IllegalArgumentException("Input file must not be null");
        }
        parkingSpots.clear();
        infile.useDelimiter("\r?\n|\r"); // End of line characters on Unix or DOS

        name = infile.next();
        priceHour = infile.nextDouble();
        totalParkingSpots = infile.nextInt();
        while(infile.hasNext() && !infile.hasNext("----------END OF ZONE----------")){

            String key = infile.next();

            if(key.equals("---ParkingSpot---")) {
                ParkingSpot parkingSpot = new ParkingSpot();
                parkingSpot.load(infile);
                parkingSpots.add(parkingSpot);
            }

            if(key.equals("carPark.vehicle.StandardVehicle")){
                typeOfAllowedVehicles.add(new StandardVehicle());
            }
            if(key.equals("carPark.vehicle.HigherVehicle")){
                typeOfAllowedVehicles.add(new HigherVehicle());
            }
            if(key.equals("carPark.vehicle.LongerVehicle")){
                typeOfAllowedVehicles.add(new LongerVehicle());
            }
            if(key.equals("carPark.vehicle.Coache")){
                typeOfAllowedVehicles.add(new Coache());
            }
            if(key.equals("carPark.vehicle.Motorbike")){
                typeOfAllowedVehicles.add(new Motorbike());
            }
        }
        infile.next(); //Clears "----------END OF ZONE----------" line
    }

    /**
     * This method prints information about the Zone
     *
     * @return a string of information
     */
    @Override
    public String toString() {
        StringBuilder zoneString = new StringBuilder();
        zoneString.append(name + ": ");
        zoneString.append(parkingSpots);
        zoneString.append("\n");

        return zoneString.toString();
    }
}
