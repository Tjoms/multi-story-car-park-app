import carPark.CarPark;
import carPark.people.Admin;
import carPark.people.Attendant;
import carPark.people.Customer;
import carPark.people.Person;
import carPark.receipt.Receipt;
import carPark.token.Token;
import carPark.vehicle.Coache;
import carPark.vehicle.Vehicle;
import carPark.zone.Zone;

import java.io.*;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * The main application class for the car park - Where everything with the command line menu happens
 *
 * @author mat80
 * @version 1.0 (09/05/2019)
 */

public class Application {

    //---------------------------------------------APPLICATION OBJECTS--------------------------------------------------

    private CarPark carPark;
    private Scanner scan;
    private ArrayList<Person> people = new ArrayList<>();
    private ArrayList<Attendant> poolOfFreeAttendants = new ArrayList<>();

    //---------------------------------------------USER OBJECTS---------------------------------------------------------

    private Customer customer;
    private Attendant attendant;
    private Admin admin;

    //---------------------------------------------APPLICATION LOGIC----------------------------------------------------

    /**
     * Constructor for application
     */
    private Application() {
        carPark = new CarPark();
        scan = new Scanner(System.in);
    }

    /**
     * This method runs the application
     */
    public static void main(String[] args) {
        Application app = new Application();
        app.initialize();
        app.mainMenu();
    }

    //---------------------------------------------INITIALIZE LOGIC-----------------------------------------------------

    /**
     * This method initializes everything needed for the car park and application
     */
    private void initialize() {
        carPark.initialize();
        tryLoadPeople();
    }


    //---------------------------------------------MAIN MENU LOGIC------------------------------------------------------

    /**
     * This method displays the main menu to the user and lets the user choose what to do
     */
    private void mainMenu() {

        String choice;
        do {
            printMainMenu();
            choice = scan.next();

            switch (choice) {
                case "1":
                    login();
                    break;
                case "2":
                    createNewCustomer();
                    break;
            }
        } while (!choice.toUpperCase().equals("Q"));
    }

    /**
     * This method prints the main menu options to the console
     */
    private void printMainMenu() {
        System.out.println("-----WELCOME TO THE CAR PARK----- \n");
        System.out.println("1 - login");
        System.out.println("2 - create new customer");
        System.out.println("q - Quit program");
    }


    //---------------------------------------------LOGIN LOGIC---------------------------------------------------------

    /**
     * This method enables the user to login to the car park
     */
    private void login() {

        customer = null;
        attendant = null;
        admin = null;

        loginMenu();
        loadUserMenu();
    }

    /**
     * This method lets the user enter username and password
     */
    private void loginMenu() {
        System.out.println("-----LOGIN MENU----- \n");

        System.out.println("Enter username: ");
        String name = scan.next();
        System.out.println("Enter password:");
        String password = scan.next();

        lookForUser(name, password);
    }

    /**
     * This method looks for the user entered
     *
     * @param name     is the username of the user
     * @param password is the password that is connected to the username
     */
    private void lookForUser(String name, String password) {
        boolean userExists = false;
        for (Person person : people) {
            if (person.getName().equals(name)) {
                if (person.getPassword().equals(password)) {

                    setUser(person);
                    userExists = true;
                    break;
                }

                if (!person.getPassword().equals(password)) {
                    System.out.println("Username and password does not match");
                    userExists = false;
                    break;
                }
            }
        }

        if (!userExists) {
            if (customer == null && attendant == null && admin == null) {
                System.out.println("This user does not exist \n");
            }
        }
    }

    /**
     * This method sets the user to be either:
     * - A customer
     * - An attendant
     * - An admin
     *
     * @param person is the current user
     */
    private void setUser(Person person) {
        if (person.getClass().equals(Customer.class)) {
            customer = (Customer) person;
        }
        if (person.getClass().equals(Attendant.class)) {
            attendant = (Attendant) person;
        }
        if (person.getClass().equals(Admin.class)) {
            admin = (Admin) person;
        }
    }

    /**
     * This method loads different menus depending on what type of people the user is
     */
    private void loadUserMenu() {
        if (customer != null) {
            mainMenuCustomer();
        }

        if (attendant != null) {
            mainMenuAttendant();
        }

        if (admin != null) {
            mainMenuAdmin();
        }
    }

    /**
     * This method lets the user create a new customer
     */
    private void createNewCustomer() {
        System.out.println("-----CREATE NEW CUSTOMER----- \n");
        System.out.println("Enter your username: ");
        String name = scan.next();

        boolean userAlreadyExist = false;
        userAlreadyExist = doesUserAlreadyExist(name, userAlreadyExist);


        if (!userAlreadyExist) {
            Customer customer = addInformationToCustomer(name);
            people.add(customer);
            trySavePeople();
            System.out.println("Successfully added " + customer.getName());
        }
    }

    private boolean doesUserAlreadyExist(String name, boolean userAlreadyExist) {
        for (Person person : people) {
            if (person.getName().equals(name)) {
                System.out.println("This username is already taken \n");
                userAlreadyExist = true;
            }
        }
        return userAlreadyExist;
    }

    /**
     * This method sets the information to the new customer
     */
    private Customer addInformationToCustomer(String name) {
        System.out.println("Enter your password: ");
        String password = scan.next();
        System.out.println("Are you disabled?(Y/N)");
        boolean answer = isUserInputYes();

        boolean isDisabled = false;
        if (answer) {
            isDisabled = true;
        }
        return new Customer(name, password, isDisabled);
    }


    //---------------------------------------------CUSTOMER LOGIC------------------------------------------------------

    /**
     * This method lets the Customer decide what to do in the car park
     */
    private void mainMenuCustomer() {

        System.out.println("Welcome " + customer.getName());
        String choice;
        do {
            printMainMenuCustomer();
            choice = scan.next();

            switch (choice) {
                case "1":
                    checkIn();
                    break;
                case "2":
                    checkOut();
                    break;
                case "3":
                    displayParkingSpots();
                    break;
                case "4":
                    displayCustomerReceipt();
                    break;
            }

        } while (!choice.toUpperCase().equals("Q"));

    }

    /**
     * This method prints out the choices the Customer have in the car park
     */
    private void printMainMenuCustomer() {
        System.out.println("-----CUSTOMER MENU----- \n");
        System.out.println("1 - check in");
        System.out.println("2 - check out");
        System.out.println("3 - display Zones");
        System.out.println("4 - display your receipt");
        System.out.println("q - Exit customer menu");
    }

    /**
     * This method lets the Customer check in their vehicle
     */
    private void checkIn() {
        System.out.println("-----CHECK IN----- \n");

        if (customer.getReceipt() == null && customer.getVehicle() == null && !hasAttendantCustomer()) {
            Vehicle vehicle = addInformationToVehicle();
            vehicle = vehicle.getTypeOfVehicle(vehicle);

            if (vehicle != null) {
                if (Attendant.canParkThisVehicle(vehicle.getClass())) {
                    askUserForAttendantCheckIn(vehicle);

                } else {
                    parkAsCustomer(vehicle);

                }
                trySavePeople();
            }
        } else {
            System.out.println("You already have a car parked or waiting to get parked, please check out before you park a new vehicle \n");
        }
    }

    /**
     * This method checks whether or not the Customer is already waiting for an attendant to park their vehicle
     */
    private boolean hasAttendantCustomer() {
        boolean hasAttendantCustomer = false;
        for (Person person : people) {
            if (person instanceof Attendant) {
                String customerName = customer.getName();
                String attendantCustomer = ((Attendant) person).getCustomerName();
                if (customerName.equals(attendantCustomer)) {
                    hasAttendantCustomer = true;
                }
            }
        }
        return hasAttendantCustomer;
    }

    /**
     * This method ask the Customer whether or not it wants an Attendant to park their vehicle
     *
     * @param vehicle is the vehicle going to be parked
     */
    private void askUserForAttendantCheckIn(Vehicle vehicle) {
        System.out.println("There are currently " + poolOfFreeAttendants.size() + " Attendants available");
        System.out.println("Want Attendant to park your vehicle? (Y/N)");
        boolean answer = isUserInputYes();

        if (answer) {
            tryAttendantCheckIn(vehicle);
        } else {
            parkAsCustomer(vehicle);
        }
    }

    /**
     * This method lets the Customer try to send their vehicle to an Attendant
     *
     * @param vehicle is vehicle going to be parked
     */
    private void tryAttendantCheckIn(Vehicle vehicle) {
        if (!poolOfFreeAttendants.isEmpty()) {

            attendantCheckIn(vehicle);

        } else {
            System.out.println("All attendants are busy, park manually");
            parkAsCustomer(vehicle);
        }
    }

    /**
     * This method lets the Customer send their vehicle to an Attendant
     *
     * @param vehicle is vehicle going to be parked
     */
    private void attendantCheckIn(Vehicle vehicle) {
        Attendant attendant = poolOfFreeAttendants.get(0);
        poolOfFreeAttendants.remove(attendant);

        attendant.setCustomerName(customer.getName());
        attendant.setVehicle(vehicle);


        moveAttendantToLastSpotInArray(attendant);

        System.out.println("Your attendant is " + attendant.getName());
        System.out.println("Wait for your attendant to park your vehicle \n");
    }

    /**
     * This method moves the Attendant at the end of the array of people
     *
     * @param attendant is the Attendant being moved
     */
    private void moveAttendantToLastSpotInArray(Attendant attendant) {
        people.remove(attendant);
        people.add(attendant);
    }


    /**
     * This method lets the Customer enter their vehicle information
     */
    private Vehicle addInformationToVehicle() {
        System.out.println("Please enter your vehicle licence plate:");
        String licencePlate = scan.next();

        System.out.println("For motorbike type 0 for height and 0 for length");
        double height = readUserInput("Enter vehicle height");
        double length = readUserInput("Enter vehicle length");

        return new Vehicle(licencePlate, height, length);
    }


    /**
     * This method lets the Customer park their vehicle
     *
     * @param vehicle is the vehicle being parked
     */
    private void parkAsCustomer(Vehicle vehicle) {
        Receipt receipt;
        receipt = carPark.parkVehicleAsCustomer(vehicle);
        if (receipt != null) {
            carPark.addActiveReceipt(receipt);
            customer.setReceipt(receipt);
            System.out.println("Your receipt: " + customer.getReceipt());
        }
    }


    /**
     * This method lets the Customer check out of the car park
     */
    private void checkOut() {
        System.out.println("-----CHECK OUT----- \n");

        if (!hasAttendantCustomer()) {
            if (customer.getReceipt() != null) {

                System.out.println("Enter your receipt code");
                String receiptCode = scan.next();

                Receipt receiptTemp = carPark.searchForReceipt(receiptCode);

                if (receiptTemp != null) {
                    double totalUnits = getTotalAmount(receiptTemp);
                    userPaymentAndRemoveVehicle(receiptTemp, totalUnits);

                } else {
                    System.out.println("Invalid receipt code \n");
                }
            } else {
                System.out.println("You have no vehicle parked \n");
            }
        } else {
            System.out.println("An attendant is parking or getting your vehicle \n");
        }
    }

    /**
     * This method gets the total amount the user has to pay
     *
     * @param receiptTemp is the receipt used to get the start time
     */
    private double getTotalAmount(Receipt receiptTemp) {
        double totalUnits = carPark.getTotalUnits(receiptTemp);


        if (customer.isDisabled()) {

            if (receiptTemp.getVehicleClass() != Coache.class) {
                totalUnits /= 2;

                if (customer.getReceipt().getStartDateTime().getDayOfWeek().name().equals("SUNDAY")) {
                    totalUnits = 0.0;
                }

            } else {
                System.out.println("No discount for disabled customers driving coaches");
            }
        }
        return totalUnits;
    }

    /**
     * This method lets the Customer pay and exit the car park
     *
     * @param receiptTemp is the receipt used to retrieve information
     * @param totalUnits  is the total units the Customer has to pay
     */
    private void userPaymentAndRemoveVehicle(Receipt receiptTemp, double totalUnits) {
        double userPays = 0;
        do {
            System.out.println("Pay:");
            userPays = readUserInput("Total amount is: " + totalUnits + "Units");

            if (userPays >= totalUnits) {
                double change = userPays - totalUnits;
                System.out.println("Change: " + change + " Units");

                attendantOrCustomerCheckOut(receiptTemp);
                trySavePeople();
            } else {
                System.out.println("The amount is to small!");
            }
        } while (userPays < totalUnits);
    }

    /**
     * This method asks the Customer whether or not it wants Attendant checkout and does it
     *
     * @param receiptTemp is the receipt used to get information
     */
    private void attendantOrCustomerCheckOut(Receipt receiptTemp) {
        if (Attendant.canParkThisVehicle(receiptTemp.getVehicleClass())) {
            System.out.println("There are currently " + poolOfFreeAttendants.size() + " Attendants available");
            System.out.println("Want attendant to get your Vehicle? (Y/N)");
            boolean answer = isUserInputYes();

            if (answer) {
                tryAttendantCheckOut(receiptTemp);
            } else {
                removeVehicleAsCustomer(receiptTemp);
                System.out.println("Vehicle exited the car park \n");
            }
        } else {
            removeVehicleAsCustomer(receiptTemp);
            System.out.println("Vehicle exited the car park \n");
        }

    }

    /**
     * This method tries to do Attendant checkout, if it fails it will checkout as a customer
     *
     * @param receiptTemp is the receipt used to get information
     */
    private void tryAttendantCheckOut(Receipt receiptTemp) {
        if (!poolOfFreeAttendants.isEmpty()) {
            attendantCheckOut();
        } else {
            System.out.println("All attendants are busy, check out manually");
            removeVehicleAsCustomer(receiptTemp);
            System.out.println("Vehicle exited the car park \n");
        }
    }

    /**
     * This method lets the Attendant checkout the Customer vehicle
     */
    private void attendantCheckOut() {
        Attendant attendant = poolOfFreeAttendants.get(0);
        poolOfFreeAttendants.remove(attendant);
        attendant.setCustomerName(customer.getName());

        Token token = new Token();
        token.resetTimer();
        attendant.setToken(token);

        moveAttendantToLastSpotInArray(attendant);

        System.out.println("Your attendant is " + attendant.getName());
        System.out.println("Wait for an attendant to get your vehicle \n");
    }

    /**
     * This method lets the Customer checkout their vehicle
     */
    private void removeVehicleAsCustomer(Receipt receiptTemp) {
        giveToken(customer);
        carPark.removeVehicleAndReceipt(receiptTemp);
        customer.setToken(null);
        customer.setReceipt(null);

    }

    /**
     * This method gives a token to the people checking out a vehicle
     *
     * @param person is the people receiving the token
     */
    private void giveToken(Person person) {
        Token token = new Token();
        token.resetTimer();

        giveTokenToPerson(person, token);
        exitTheGarage(person, token);
    }

    /**
     * This method checks if the Person checking out have used more time than allowed
     * If you have used longer than allowed, you are supposed to seek assistance
     *
     * @param person is the people exiting the car park
     * @param token  is the token used to determine how long the Person have used to exit the car park
     */
    private void exitTheGarage(Person person, Token token) {
        boolean hasExitGarageInTime;
        do {
            System.out.println("Enter any character to exit the car park");
            scan.next();
            hasExitGarageInTime = token.hasExitGarageInTime();

            //Supposed to seek assistance, for convenience ive just reset the exit time
            if (!hasExitGarageInTime) {
                System.out.println("You waited to long to exit, seek assistance!");

                giveTokenToPerson(person, token);

                token.resetTimer();
            }

        } while (!hasExitGarageInTime);
    }

    /**
     * This method gives token to the type of Person collecting a vehicle
     *
     * @param person is the people receiving the token
     * @param token  is the used to exit the car park
     */
    private void giveTokenToPerson(Person person, Token token) {
        if (person.getClass().equals(Customer.class)) {
            ((Customer) person).setToken(token);
        }

        if (person.getClass().equals(Attendant.class)) {
            ((Attendant) person).setToken(token);
        }
    }

    /**
     * This method displays the current state of the car park
     */
    private void displayParkingSpots() {
        System.out.println("-----CAR PARK STATUS----- \n");
        carPark.zoneDisplay();
    }

    /**
     * This method displays the Customer's receipt
     */
    private void displayCustomerReceipt() {
        System.out.println("-----RECEIPT----- \n");
        Receipt receipt = customer.getReceipt();
        if (receipt != null) {
            System.out.println("Your receipt: " + receipt + "\n");
        } else
            System.out.println("You have no receipt \n");
    }


    //---------------------------------------------ATTENDANT LOGIC-----------------------------------------------------

    /**
     * This method lets the Attendant decide what to do in the car park
     */
    private void mainMenuAttendant() {

        String choice;
        do {
            printMainMenuAttendant();
            choice = scan.next();
            switch (choice) {
                case "1":
                    checkInWithAttendant();
                    break;
                case "2":
                    checkOutWithAttendant();
                    break;
                case "3":
                    displayParkingSpots();
                    break;
            }
        } while (!choice.equals("q"));
    }

    /**
     * This method prints out choices the Attendant have in the car park
     */
    private void printMainMenuAttendant() {
        System.out.println("-----ATTENDANT MENU----- \n");
        System.out.println("1 - check in customer");
        System.out.println("2 - check out customer");
        System.out.println("3 - display Zones");
        System.out.println("q - Exit attendant menu");
    }

    /**
     * This method checks in a Customer as an Attendant
     */
    private void checkInWithAttendant() {
        System.out.println("-----CHECK IN----- \n");
        if(!attendant.getCustomerName().equals("null")) {
            if (attendant.getVehicle() != null) {
                parkVehicleAsAttendant();
                trySavePeople();
            } else {
                System.out.println("You have currently no vehicle that needs parking \n");
            }
        } else{
            System.out.println("You do not have a customer at the moment \n");
        }
    }

    /**
     * This method parks the vehicle as an Attendant
     */
    private void parkVehicleAsAttendant() {
        try {
            Vehicle vehicle = attendant.getVehicle();
            carPark.searchForAllowedZonesToParkIn(vehicle);

            Receipt receipt = lookForParkingSpotAndParkAndGetReceipt(vehicle);
            giveReceiptToCustomer(receipt);

        } catch (IndexOutOfBoundsException e) {
            System.out.println("This spot does not exist \n");
        }
    }

    /**
     * This method lets the Attendant search for parking spots and park in one of them
     *
     * @param vehicle is the vehicle being parked
     * @return receipt after parking is completed
     */
    private Receipt lookForParkingSpotAndParkAndGetReceipt(Vehicle vehicle) {
        System.out.println("Enter zone you want to park in");
        String zoneName = scan.next();

        int spotID = -1; //-1 is not a spotID, will give error if not changed
        boolean error;
        do {
            System.out.println("Enter the spot you want to park in \n");
            try {
                spotID = scan.nextInt();
                error = false;
            } catch (InputMismatchException e) {
                System.err.println("Please enter a number \n");
                scan.next();
                error = true;
            }
        } while (error);

        Receipt receipt = carPark.parkVehicleAsAttendant(vehicle, zoneName, spotID);
        return receipt;
    }

    /**
     * This method gives the receipt to the Customer and resets the status of the current Attendant
     *
     * @param receipt is the receipt given to the Customer
     */
    private void giveReceiptToCustomer(Receipt receipt) {
        if (receipt != null) {
            for (Person person : people) {
                if (person instanceof Customer) {
                    if (person.getName().equals(attendant.getCustomerName())) {
                        ((Customer) person).setReceipt(receipt);
                        carPark.addActiveReceipt(receipt);

                        System.out.println(attendant.getName() + " successfully parked " + attendant.getCustomerName() + "'s vehicle \n");
                        resetAttendantAndAddToPoolOfFreeAttendants();
                    }
                }
            }

        } else {
            System.out.println("Try a different zone name or spot id");
        }
    }

    /**
     * This method resets the Attendant status and adds it to the pool of free attendants
     */
    private void resetAttendantAndAddToPoolOfFreeAttendants() {
        attendant.setCustomerName(null);
        attendant.setVehicle(null);
        attendant.setToken(null);
        poolOfFreeAttendants.add(attendant);


    }

    /**
     * This method lets the Attendant checkout a Customer vehicle
     */
    private void checkOutWithAttendant() {
        System.out.println("-----CHECK OUT----- \n");
        if (!attendant.getCustomerName().equals("null")) {
            if (attendant.getVehicle() == null) {
                for (Person person : people) {
                    if (person instanceof Customer) {
                        if (person.getName().equals(attendant.getCustomerName())) {
                            Receipt receipt = ((Customer) person).getReceipt();

                            exitTheGarage(attendant, attendant.getToken());
                            carPark.removeVehicleAndReceipt(receipt);
                            ((Customer) person).setReceipt(null);

                            resetAttendantAndAddToPoolOfFreeAttendants();
                            System.out.println(person.getName() + " vehicle was removed by attendant " + attendant.getName());
                        }
                    }
                }
            } else {
                System.out.println("You are not supposed to check out, try check in \n");
            }
        } else {
            System.out.println("You do not have a customer at the moment \n");
        }
        trySavePeople();

    }


    //---------------------------------------------ADMIN LOGIC----------------------------------------------------------

    /**
     * This method lets the Admin decide what to do in the car park
     */
    private void mainMenuAdmin() {
        System.out.println("Welcome " + admin.getName());
        String choice;
        do {
            printMainMenuAdmin();
            choice = scan.next();

            switch (choice) {
                case "1":
                    createNewAttendant();
                    break;
                case "2":
                    createNewAdmin();
                    break;
                case "3":
                    displayParkingSpots();
                    break;
                case "4":
                    editZonePrice();
                    break;
                case "5":
                    displayActiveReceipt();
                    break;
                case "6":
                    removeAttendant();
                    break;
                case "7":
                    removeAdmin();
                    break;
            }

        } while (!choice.toUpperCase().equals("Q"));

    }

    /**
     * This method prints out the choices the Admin have in the car park
     */
    private void printMainMenuAdmin() {
        System.out.println("-----ADMIN MENU---- \n");
        System.out.println("1 - create new Attendant");
        System.out.println("2 - create new Admin");
        System.out.println("3 - display Zones");
        System.out.println("4 - edit zone price");
        System.out.println("5 - display all active receipts");
        System.out.println("6 - remove Attendant");
        System.out.println("7 - remove Admin");
        System.out.println("q - Exit Admin menu");
    }

    /**
     * This method creates a new Attendant
     */
    private void createNewAttendant() {
        System.out.println("-----CREATE NEW ATTENDANT----- \n");

        System.out.println("Enter attendant's username: ");
        String name = scan.next();


        boolean userAlreadyExist = false;
        userAlreadyExist = doesUserAlreadyExist(name, userAlreadyExist);


        if (!userAlreadyExist) {
            System.out.println("Enter attendant's password: ");
            String password = scan.next();
            Attendant attendant = new Attendant(name, password);
            people.add(attendant);

            System.out.println("Successfully added " + attendant.getName());
        }

        trySavePeople();

    }

    /**
     * This method removes an Attendant of the Admins choice
     */
    private void removeAttendant() {
        System.out.println("-----REMOVE ATTENDANT----- \n");
        System.out.println("Enter the name of the Attendant you want to remove: ");
        String name = scan.next();

        Person personToRemove = null;

        for (Person person : people) {
            if (person instanceof Attendant) { //Just in case you type in another persons username and you suddenly remove someone you're not supposed to remove
                if (person.getName().equals(name)) {
                    personToRemove = person;
                    break;
                }
            }
        }

        removePerson(personToRemove);

    }

    /**
     * This method removes a people from the people ArrayList
     *
     * @param personToRemove is the people being removed
     */
    private void removePerson(Person personToRemove) {
        if (personToRemove != null) {
            people.remove(personToRemove);
            System.out.println("Successfully removed: " + personToRemove.getName());
            trySavePeople();
        } else {
            System.out.println("Could not find the person you where looking for");
        }
    }

    /**
     * This method creates a new Admin
     */
    private void createNewAdmin() {
        System.out.println("-----CREATE NEW ADMIN----- \n");
        System.out.println("Enter admin's username: ");
        String name = scan.next();

        boolean userAlreadyExist = false;
        userAlreadyExist = doesUserAlreadyExist(name, userAlreadyExist);


        if (!userAlreadyExist) {
            System.out.println("Enter admin's password: ");
            String password = scan.next();
            Admin admin = new Admin(name, password);
            people.add(admin);

            System.out.println("Successfully added " + admin.getName());
        }
        trySavePeople();

    }

    /**
     * This method removes an Admin, but not the current one
     */
    private void removeAdmin() {
        System.out.println("-----REMOVE ADMIN----- \n");
        System.out.println("Enter the name of the Admin you want to remove: ");
        String name = scan.next();

        Person personToRemove = null;

        for (Person person : people) {
            if (person instanceof Admin) {
                if (person.getName().equals(name)) {
                    if (!person.getName().equals(admin.getName())) {
                        personToRemove = person;
                        break;

                    } else {
                        System.out.println("You cannot remove yourself");
                    }
                }
            }
        }

        removePerson(personToRemove);
    }

    /**
     * This method enables the Admin to edit zone prices
     */
    private void editZonePrice() {
        System.out.println("-----EDIT ZONE PRICE----- \n");
        System.out.println("Enter the name of the zone you want to change price/hour");
        String zoneName = scan.next();

        for (Zone zone : carPark.getZones()) {
            if (zone.getName().equals(zoneName)) {

                System.out.println(zone.getName() + " currently has " + zone.getPriceHour() + " Units per hour");

                System.out.println("Do you wish to edit price/hour? (Y/N)");
                boolean answer = isUserInputYes();
                if (answer) {
                    editPriceHour(zone);
                }
            }
        }
    }

    /**
     * This method edits the price/hour for a zone
     *
     * @param zone is the zone being edited
     */
    private void editPriceHour(Zone zone) {
        double newPrice = readUserInput("Enter the new price/hour");

        zone.setPriceHour(newPrice);

        carPark.trySaveZones();
        System.out.println("New price/hour for " + zone.getName() + " is: " + zone.getPriceHour() + "\n");

    }

    /**
     * This method displays all active receipts in the car park
     */
    private void displayActiveReceipt() {
        System.out.println("-----DISPLAY ALL ACTIVE RECEIPTS----- \n");
        try {
            System.out.println("Total Active Receipts: " + carPark.getActiveReceipts());
        } catch (NullPointerException e) {
            System.out.println("There are no active receipts \n");
        }
    }


    //---------------------------------------------LOADING & SAVING-----------------------------------------------------

    /**
     * This method tries to load people
     */
    private void tryLoadPeople() {
        try {
            String filename = "people.txt";
            loadPeople(filename);
        } catch (IOException e) {
            System.out.println("Error loading people");
        }
    }

    /**
     * This method tries to load multiple Persons and adds them to a list of people
     *
     * @throws IOException
     */
    private void loadPeople(String filename) throws IOException {

        try (FileReader fr = new FileReader(filename);
             BufferedReader br = new BufferedReader(fr);
             Scanner infile = new Scanner(br)) {

            while (infile.hasNext()) {
                String typeOfPerson = infile.next();
                Person person = null;

                switch (typeOfPerson) {
                    case "-----Admin-----":
                        person = new Admin();
                        break;
                    case "-----Attendant-----":
                        person = new Attendant();
                        break;
                    case "-----Customer-----":
                        person = new Customer();
                        break;
                }

                if (person != null) {
                    person.load(infile);
                    people.add(person);
                    addAttendantToPoolOfFreeAttendants(person);
                    addReceiptToCustomer(person);
                }
            }
        }
    }

    /**
     * This method adds a Attendant to a list of free attendants
     *
     * @param person is the Attendant being added to a list
     */
    private void addAttendantToPoolOfFreeAttendants(Person person) {
        if (person instanceof Attendant)
            if (person.getVehicle() == null && ((Attendant) person).getCustomerName().equals("null")) {
                poolOfFreeAttendants.add((Attendant) person);
            }
    }

    /**
     * This method gives the Customer a receipt and adds it to a list of active receipts
     *
     * @param person is the Customer getting the receipt
     */
    private void addReceiptToCustomer(Person person) {
        if (person instanceof Customer) {
            Receipt receipt = ((Customer) person).getReceipt();
            if (receipt != null) {
                carPark.addActiveReceipt(receipt);
            }
        }
    }

    /**
     * This method tries to saves people
     */
    private void trySavePeople() {
        try {
            String outfileName = "people.txt";
            savePeople(outfileName);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * This method saves people to a txt file
     *
     * @param outfileName is the name of the txt file
     * @throws IOException
     */
    private void savePeople(String outfileName) throws IOException {

        try (FileWriter fw = new FileWriter(outfileName);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter outfile = new PrintWriter(bw)) {

            for (Person person : people) {
                person.save(outfile);
            }
        }
    }

    //---------------------------------------------ANSWER CHECKING------------------------------------------------------

    /**
     * This method reads the user input for yes/no questions
     * It only accepts the inputs 'Y', 'y' or 'N', 'n'
     *
     * @return true/false
     */
    private boolean isUserInputYes() {
        boolean tryAgain;
        boolean userInput = false;
        do {
            String choice = scan.next().toUpperCase();

            switch (choice) {
                case "Y":
                    userInput = true;
                    tryAgain = false;
                    break;
                case "N":
                    userInput = false;
                    tryAgain = false;
                    break;
                default:
                    System.err.println(" Did not understand the input, try again");
                    tryAgain = true;
            }
        } while (tryAgain);
        return userInput;
    }

    /**
     * This method returns a double
     *
     * @param message is the message printed to the user
     * @return a double
     */
    private double readUserInput(String message) {
        double number = 0;
        boolean inputIsDouble;
        do {
            try {
                System.out.println(message);
                number = scan.nextDouble();
                inputIsDouble = true;
            } catch (InputMismatchException e) {
                System.err.println("Please enter a number \n");
                scan.next();
                inputIsDouble = false;
            }
        } while (!inputIsDouble);

        return number;
    }
}
